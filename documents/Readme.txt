a) General

-- The workspace includes two Spring Boot - RESTful API microservices for the PEP program, along the Postman collections utilised to
test the functionality set, both during and after their development;
-- They run both locally and on a minikube instance after following the commands list posted on the chapter below (b);
-- The code includes comments for all key points, including what could have been done (a bit) differently
from my point of view, in relation to the proposed model/requirements;
-- If any issues in running these microservices pop up or any additional questions arise, please do not hesitate to contact
me at: mistan@pentalog.com

b) Installation

In order to be able to run the microservices locally, one only needs an IDE for Java, such as IntelliJ, Eclipse or NetBeans;
As for Kubernetes, one needs either:
Docker - includes Kubernetes within the "Desktop" version, albeit a slightly older version, available for MacOS and Windows: https://hub.docker.com/?overlay=onboarding
or
Minikube - https://kubernetes.io/docs/tasks/tools/install-minikube
Hyper-V - https://techcommunity.microsoft.com/t5/itops-talk-blog/step-by-step-enabling-hyper-v-for-use-on-windows-10/ba-p/267945
Kubectl - https://kubernetes.io/docs/tasks/tools/install-kubectl

As one could notice above, the installation steps for Minikube and Kubectl are pretty similar for all operating systems, but the commands are not identical.
Having that said, please choose the right solution for the right OS;

c) Configuration

I. Local run

1. Configure postgres database by:
	a) Defining a new role/user:
		CREATE ROLE pgadmin SUPERUSER CREATEDB NOCREATEROLE INHERIT LOGIN PASSWORD 'pgadmin';
	b) Create a new database, called pgdb, with the owner set to pgadmin:
		CREATE DATABASE pgdb OWNER pgadmin
2. Set the environment variable for the "campaignapi" microservice by running:
	a) a cmd instance for Windows with the following command: setx CAMPAIGN_API_ADDRESS http://localhost:8080/campaign
	b) a terminal instance for UNIX/Linux: export CAMPAIGN_API_ADDRESS=http://localhost:8080/campaign
3. Start the Spring Boot applications from an IDE by running the classes "CampaignApplication" and "PointsApplication".
4. Access the two microservices using the following base endpoints:
	http://localhost:8080/campaign
	http://localhost:8081/points
	
II. "Cloud" (minikube)

-- Maven
run mvn clean install (artifacts should be built with the next names -> campaignapi-1.0.0-SNAPSHOT.jar / pointsapi-1.0.0-SNAPSHOT.jar)
* either from CMD/Terminal, going in the root folder of each microservice to access the pom.xml file directly, or via IDE

-- Kubernetes (minikube)/Docker
minikube start

kubectl config use-context minikube

minikube docker-env
@FOR /f "tokens=*" %i IN ('minikube docker-env') DO @%i

cd <PATH_TO_MICROSERVICES_WORKSPACE_ROOT_FOLDER>

docker build -t campaignapi .\campaignapi
docker build -t pointsapi .\pointsapi

kubectl apply -f postgresdb\k8s\postgres-volume.yaml
kubectl apply -f postgresdb\k8s\postgres-secret-config.yaml
kubectl apply -f postgresdb\k8s\postgres-scaler.yaml
kubectl apply -f postgresdb\k8s\postgres-deployment-service.yaml

kubectl apply -f campaignapi\k8s\campaign-secret-config.yaml
kubectl apply -f campaignapi\k8s\postgres-scaler.yaml
kubectl apply -f campaignapi\k8s\campaign-deployment-service.yaml

kubectl apply -f pointsapi\k8s\points-map-config.yaml
kubectl apply -f pointsapi\k8s\points-secret-config.yaml
kubectl apply -f pointsapi\k8s\points-scaler.yaml
kubectl apply -f pointsapi\k8s\points-deployment-service.yaml
kubectl apply -f pointsapi\k8s\points-clusterrole-config.yaml

# openssl req -x509 -newkey rsa:4096 -sha256 -nodes -keyout gateway\certificate\tls.key -out gateway\certificate\tls.crt -subj "/CN=pep-microservices.info" -days 3650
kubectl create secret tls pep-microservices-info-tls --key gateway\certificate\tls.key --cert gateway\certificate\tls.crt
kubectl apply -f gateway\k8s\oauth2-deployment-service.yaml
kubectl apply -f gateway\k8s\pep-gateway-ingress.yaml
kubectl run nginx --image=nginx
kubectl expose deployment nginx --port 80

# one needs to know the pods names in order to start the port-forwarding on localhost:8080/8081 and access PSQL capabilities
kubectl get pods
start /min kubectl port-forward <campaignapi-xyzxyzxyz-xyzxy> 8080:8080
start /min kubectl port-forward <pointsapi-xyzxyzxyz-xyzxy> 8081:8080

# Run commands against the database and Postman on the same URLs as before to check the same functionality on minikube
# This, however, requires a NodeIP on the service config (now set to "ClusterIP"), otherwise the database cannot be reached outside the cluster.
kubectl exec -it <postgres-service-xyzxyzxyz-xyzxy> bash
psql -h localhost -U postgres --password -p 5432 postgresdb
Password: postgres

-- For the entire commands list used to develop/test these microservices on minikube, please check the file "Commands.txt".
-- There you will also find how to leave out some parts of the implementation, if desired
-- For secrets data encryption, please check this URL: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data
-- For understanding how role based access control works in order to be able to communicate between microservices, please check this URL: https://kubernetes.io/docs/reference/access-authn-authz/rbac