package com.pep.points.supplier;

import com.pep.points.dto.CampaignDto;
import com.pep.points.entity.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class PointsSupplier {

    public static Optional<PointsBalance> supplyPointsBalanceForCustomer() {
        PointsBalance pointsBalance = new PointsBalance();
        pointsBalance.setId(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c493"));
        pointsBalance.setPoints(4);
        pointsBalance.setCustomerEmail("testEmail@gmail.com");

        return Optional.of(pointsBalance);
    }

    public static Optional<PointsTransaction> supplyPointsTransactionForGetAndInsert() {
        Product product = new Product();
        product.setId(UUID.randomUUID());
        product.setName("testProduct");
        product.setPrice(BigDecimal.valueOf(60));

        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setQuantity(3);
        orderItem.setProduct(product);

        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem);

        Order order = new Order();
        order.setPoints(40);
        order.setCustomerEmail("testEmail@gmail.com");
        order.setCreatedAt(LocalDate.of(2020, 01, 20));
        order.setItems(orderItems);

        PointsTransaction pointsTransaction = new PointsTransaction();
        pointsTransaction.setId(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c391"));
        pointsTransaction.setOrder(order);

        return Optional.of(pointsTransaction);
    }

    public static Product supplyProductForInsert() {
        Product product = new Product();
        product.setId(UUID.randomUUID());
        product.setName("testProduct");
        product.setPrice(BigDecimal.valueOf(100));

        return product;
    }

    public static CampaignDto[] supplyCampaignDtoArrayForInsert() {
        return new CampaignDto[0];
    }

    public static PointsTransaction supplyUpdatedPointsTransactionForEdit() {
        Product product = new Product();
        product.setId(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c440"));
        product.setName("updatedTestProduct");
        product.setPrice(BigDecimal.valueOf(60));

        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c100"));
        orderItem.setQuantity(3);
        orderItem.setProduct(product);

        List<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem);

        Order order = new Order();
        order.setPoints(70);
        order.setCustomerEmail("updatedTestEmail@gmail.com");
        order.setCreatedAt(LocalDate.of(2020, 06, 16));
        order.setItems(orderItems);

        PointsTransaction pointsTransaction = new PointsTransaction();
        pointsTransaction.setId(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c391"));
        pointsTransaction.setOrder(order);

        return pointsTransaction;
    }
}
