package com.pep.points;

import com.pep.points.config.PropertiesConfig;
import com.pep.points.dto.CampaignDto;
import com.pep.points.dto.OrderDto;
import com.pep.points.dto.PointsTransactionDto;
import com.pep.points.entity.*;
import com.pep.points.exception.ResourceAlreadyExistsException;
import com.pep.points.exception.ResourceNotFoundException;
import com.pep.points.repository.*;
import com.pep.points.service.PointsService;
import com.pep.points.service.impl.PointsServiceImpl;
import com.pep.points.supplier.PointsSupplier;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static com.pep.points.adapter.PointsTransactionAdapter.toPointsTransactionDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.AssertionErrors.assertNotNull;

/**
 * This class defines the unit tests for CampaignAPI
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class PointsTransactionApplicationTests {

    public PointsTransactionApplicationTests() {
    }

    @Mock
    private PointsTransactionRepository pointsTransactionRepository = mock(PointsTransactionRepository.class);

    @Mock
    private PointsBalanceRepository pointsBalanceRepository = mock(PointsBalanceRepository.class);

    @Mock
    private ProductRepository productRepository = mock(ProductRepository.class);

    @Mock
    private OrderRepository orderRepository = mock(OrderRepository.class);

    @Mock
    private OrderItemRepository orderItemRepository = mock(OrderItemRepository.class);

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private PropertiesConfig propertiesConfig = mock(PropertiesConfig.class);

    @InjectMocks
    private PointsService pointsService = new PointsServiceImpl();

    @Test
    public void getPointsTransactionById() {
        final Optional<PointsTransaction> suppliedPointsTransaction = PointsSupplier.supplyPointsTransactionForGetAndInsert();

        doReturn(suppliedPointsTransaction).when(pointsTransactionRepository).findById(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c391"));

        PointsTransactionDto searchedPointsTransactionDto = pointsService.findPointsTransaction(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c391"));
        OrderDto searchedOrder = searchedPointsTransactionDto.getOrder();
        OrderDto suppliedOrder = toPointsTransactionDto(suppliedPointsTransaction.get()).getOrder();

        assertNotNull("Searched points transaction is null!", searchedPointsTransactionDto);
        assertEquals(searchedOrder.getCreatedAt(), suppliedOrder.getCreatedAt());
        assertEquals(searchedOrder.getPoints(), suppliedOrder.getPoints());
        assertEquals(searchedOrder.getCustomerEmail(), suppliedOrder.getCustomerEmail());
    }

    @Test
    public void getPointsBalanceByEmail() {
        final Optional<PointsBalance> suppliedPointsBalance = PointsSupplier.supplyPointsBalanceForCustomer();

        doReturn(suppliedPointsBalance).when(pointsBalanceRepository).findByCustomerEmail("testEmail@gmail.com");

        PointsBalance searchedPointsBalance = pointsService.findPointsBalanceByCustomerEmail("testEmail@gmail.com");
        assertNotNull("Searched points balance is null!", searchedPointsBalance);
        assertEquals(searchedPointsBalance, suppliedPointsBalance.get());
    }

    @Test
    public void addPointsTransaction() {
        final Optional<PointsTransaction> suppliedPointsTransaction = PointsSupplier.supplyPointsTransactionForGetAndInsert();
        final Product suppliedProduct = PointsSupplier.supplyProductForInsert();
        final Optional<PointsBalance> suppliedPointsBalance = PointsSupplier.supplyPointsBalanceForCustomer();
        final CampaignDto[] campaignArray = PointsSupplier.supplyCampaignDtoArrayForInsert();

        ResponseEntity<CampaignDto[]> responseEntity = new ResponseEntity<CampaignDto[]>(campaignArray, HttpStatus.OK);

        doReturn(false).when(orderRepository).existsByCustomerEmailAndCreatedAt("testEmail@gmail.com", LocalDate.of(2020, 01, 20));
        doReturn("testURL").when(propertiesConfig).getCampaignAPIAddress();
        doReturn(responseEntity).when(restTemplate).exchange(ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class), ArgumentMatchers.any(), ArgumentMatchers.<Class<Object>>any(), ArgumentMatchers.anyMap());
        doReturn(true).when(productRepository).existsById(suppliedProduct.getId());
        doReturn(suppliedProduct).when(productRepository).getOne(suppliedProduct.getId());
        doReturn(suppliedProduct).when(productRepository).save(suppliedProduct);
        doReturn(suppliedPointsBalance).when(pointsBalanceRepository).findByCustomerEmail("testEmail@gmail.com");
        doReturn(suppliedPointsBalance.get()).when(pointsBalanceRepository).save(suppliedPointsBalance.get());
        doReturn(suppliedPointsTransaction.get()).when(pointsTransactionRepository).save(suppliedPointsTransaction.get());

        pointsService.addPointsTransaction(suppliedPointsTransaction.get());
    }

    @Test
    public void editPointsTransaction() {
        final Optional<PointsTransaction> suppliedPointsTransaction = PointsSupplier.supplyPointsTransactionForGetAndInsert();
        final PointsTransaction suppliedUpdatedPointsTransaction = PointsSupplier.supplyUpdatedPointsTransactionForEdit();
        final Product suppliedProduct = PointsSupplier.supplyProductForInsert();
        final Optional<PointsBalance> suppliedPointsBalance = PointsSupplier.supplyPointsBalanceForCustomer();
        final CampaignDto[] campaignArray = PointsSupplier.supplyCampaignDtoArrayForInsert();

        final OrderItem suppliedUpdatedOrderItem = suppliedUpdatedPointsTransaction.getOrder().getItems().get(0);
        final ResponseEntity<CampaignDto[]> responseEntity = new ResponseEntity<CampaignDto[]>(campaignArray, HttpStatus.OK);

        doReturn(true).when(pointsTransactionRepository).existsById(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c391"));
        doReturn(suppliedPointsTransaction.get()).when(pointsTransactionRepository).getOne(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c391"));
        doReturn(true).when(orderItemRepository).existsById(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c100"));
        doReturn(suppliedUpdatedOrderItem).when(orderItemRepository).getOne(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c100"));
        doReturn(suppliedUpdatedOrderItem.getProduct()).when(productRepository).getOne(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c440"));
        doReturn("testURL").when(propertiesConfig).getCampaignAPIAddress();
        doReturn(responseEntity).when(restTemplate).exchange(ArgumentMatchers.anyString(),
                ArgumentMatchers.any(HttpMethod.class), ArgumentMatchers.any(), ArgumentMatchers.<Class<Object>>any(), ArgumentMatchers.anyMap());
        doReturn(true).when(productRepository).existsById(suppliedProduct.getId());
        doReturn(suppliedProduct).when(productRepository).getOne(suppliedProduct.getId());
        doReturn(suppliedProduct).when(productRepository).save(suppliedProduct);
        doReturn(suppliedPointsBalance).when(pointsBalanceRepository).findByCustomerEmail("updatedTestEmail@gmail.com");
        doReturn(suppliedPointsBalance.get()).when(pointsBalanceRepository).save(suppliedPointsBalance.get());
        doReturn(suppliedUpdatedPointsTransaction).when(pointsTransactionRepository).save(suppliedPointsTransaction.get());

        final PointsTransaction updatedPointsTransaction = pointsService.editPointsTransaction(suppliedPointsTransaction.get().getId(), suppliedUpdatedPointsTransaction);
        final Order suppliedUpdatedOrder = suppliedUpdatedPointsTransaction.getOrder();
        final Order updatedOrder = updatedPointsTransaction.getOrder();

        assertEquals(updatedPointsTransaction.getId(), suppliedPointsTransaction.get().getId());
        assertEquals(updatedOrder.getCreatedAt(), suppliedUpdatedOrder.getCreatedAt());
        assertEquals(updatedOrder.getPoints(), suppliedUpdatedOrder.getPoints());
        assertEquals(updatedOrder.getCustomerEmail(), suppliedUpdatedOrder.getCustomerEmail());
        assertEquals(updatedOrder.getItems(), suppliedUpdatedOrder.getItems());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void getNonExistentPointsTransaction() {
        UUID searchedId = UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c395");

        doThrow(new ResourceNotFoundException("PointsTransaction", "ID", searchedId)).
                when(pointsTransactionRepository).findById(searchedId);

        pointsService.findPointsTransaction(searchedId);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void getNonExistentPointsBalance() {
        String searchedEmail = "nonexistentemail@gmail.com";

        doThrow(new ResourceNotFoundException("PointsTransaction", "ID", searchedEmail)).
                when(pointsBalanceRepository).findByCustomerEmail(searchedEmail);

        pointsService.findPointsBalanceByCustomerEmail(searchedEmail);
    }

    @Test(expected = ResourceAlreadyExistsException.class)
    public void addAlreadyExistingPointsTransaction() {
        final PointsTransaction suppliedPointsTransaction = PointsSupplier.supplyPointsTransactionForGetAndInsert().get();
        doThrow(new ResourceAlreadyExistsException("PointsTransaction", "customer email and creation time",
                suppliedPointsTransaction.getOrder().getCustomerEmail() + " and " + suppliedPointsTransaction.getOrder().
                        getCreatedAt())).
                when(orderRepository).existsByCustomerEmailAndCreatedAt("testEmail@gmail.com", LocalDate.of(2020, 01, 20));

        pointsService.addPointsTransaction(suppliedPointsTransaction);
    }


}
