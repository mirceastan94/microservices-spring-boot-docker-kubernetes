package com.pep.points;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * This class starts the points application
 *
 * @author Mircea Stan
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@RibbonClient(name = "campaignapi")
public class PointsApplication implements CommandLineRunner {

    private static final Log logger = LogFactory.getLog(PointsApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(PointsApplication.class, args);
    }

    public void run(String... args) {
        logger.info("Points service has started!");
    }

}
