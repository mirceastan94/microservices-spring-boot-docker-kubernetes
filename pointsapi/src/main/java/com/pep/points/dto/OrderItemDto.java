package com.pep.points.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.UUID;

/**
 * This is the Dto class for order item entity create/update operations
 */
@Getter
@Setter
public class OrderItemDto {

    @NotNull
    private ProductDto product;

    @NotNull
    private UUID id;

    @Positive
    private Integer quantity;

}
