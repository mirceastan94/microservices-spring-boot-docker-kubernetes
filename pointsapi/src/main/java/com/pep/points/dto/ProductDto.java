package com.pep.points.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * This is the Dto class for product entity create/update operations
 */
@Getter
@Setter
public class ProductDto {

    @Id
    private UUID id;

    @NotBlank
    private String name;

    @Positive
    private BigDecimal price;
}
