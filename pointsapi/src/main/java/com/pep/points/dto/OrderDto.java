package com.pep.points.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This is the Dto class for order entity create/update operations
 */
@Getter
@Setter
public class OrderDto {

    @NotEmpty
    private List<OrderItemDto> items = new ArrayList<>();

    @NotNull
    private UUID id;

    @NotNull
    private LocalDate createdAt;

    @NotBlank
    private String customerEmail;

    @Positive
    private Integer points;

}
