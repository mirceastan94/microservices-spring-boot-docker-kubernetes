package com.pep.points.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * This is the Dto class for points transaction entity create/update operations
 */
@Getter
@Setter
public class PointsTransactionDto {

    @NotNull
    private OrderDto order;

}
