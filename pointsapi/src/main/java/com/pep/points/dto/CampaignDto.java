package com.pep.points.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

/**
 * This is the Dto class for campaign entity create/update operations
 */
@Getter
@Setter
public class CampaignDto {

    @NotBlank
    private String name;

    @NotNull(message = "Start date cannot be null!")
    private LocalDate startDate;

    @NotNull(message = "End date cannot be null!")
    private LocalDate endDate;

    @NotNull(message = "Points cannot be null!")
    @Min(2)
    private Integer points;

    @NotEmpty
    private Set<ProductDto> products;

}
