package com.pep.points.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * This is the Dto class for product info during an active campaign
 */
@Getter
@Setter
@AllArgsConstructor
public class ProductInfoDto {

    private BigDecimal price;
    private Integer points;

}
