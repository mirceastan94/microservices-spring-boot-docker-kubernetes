package com.pep.points.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.Positive;
import java.util.UUID;

/**
 * This is the order item entity class
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "orderitem")
public class OrderItem {

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Order orders;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Product product;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-char")
    @Column(updatable = false, unique = true)
    private UUID id;

    @Positive
    private Integer quantity;

    @Override
    public String toString() {
        return "OrderItem{" +
                ", id=" + id +
                ", quantity=" + quantity +
                '}';
    }

}
