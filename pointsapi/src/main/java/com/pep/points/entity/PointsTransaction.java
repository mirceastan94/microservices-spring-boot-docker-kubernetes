package com.pep.points.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

/**
 * This is the points transaction entity class
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "pointstransaction")
public class PointsTransaction {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-char")
    @Column(updatable = false, unique = true)
    private UUID id;

    @OneToOne(mappedBy = "pointsTransaction", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    private Order order;

    public PointsTransaction(Order order) {
        this.order = order;
        this.order.setPointsTransaction(this);
    }

}
