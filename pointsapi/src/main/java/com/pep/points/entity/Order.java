package com.pep.points.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This is the orders entity class
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "orders")
public class Order {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pointsTransaction_id")
    private PointsTransaction pointsTransaction;

    @OneToMany(mappedBy = "orders", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OrderItem> items = new ArrayList<>();

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-char")
    @Column(updatable = false, unique = true)
    private UUID id;

    @NotNull(message = "Created at field cannot be null!")
    private LocalDate createdAt;

    @NotBlank
    private String customerEmail;

    @NotNull
    @PositiveOrZero
    private Integer points;

    public void setItems(List<OrderItem> items) {
        this.items = items;
        this.items.forEach(item -> item.setOrders(this));
    }

    @Override
    public String toString() {
        return "Order{" +
                ", items=" + items +
                ", id=" + id +
                ", createdAt=" + createdAt +
                ", customerEmail='" + customerEmail + '\'' +
                ", points=" + points +
                '}';
    }
}
