package com.pep.points.entity;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This is the product entity class
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "itemproduct")
public class Product {

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    List<OrderItem> items = new ArrayList<>();

    @Id
    @Type(type = "uuid-char")
    @Column(updatable = false, unique = true)
    private UUID id;

    @NotBlank
    private String name;

    @Positive
    private BigDecimal price;

    public void setItems(List<OrderItem> items) {
        this.items = items;
        this.items.forEach(item -> item.setProduct(this));
    }

}
