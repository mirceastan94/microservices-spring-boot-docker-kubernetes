package com.pep.points.config;

import com.pep.points.dto.CampaignDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Profile;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;

@FeignClient(name = "${campaign_api_name}", configuration = FeignLogConfig.class, url = "${CAMPAIGN_API_ADDRESS}")
@Profile("prod")
public interface FeignClientConfig {

    @GetMapping(value = "/campaign/active/{date}", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CampaignDto[]> getActiveCampaignsByDate(@DateTimeFormat(pattern = "yyyy-MM-dd") @PathVariable("date") LocalDate date);

}

