package com.pep.points.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * This class defines the config for the environment properties
 */
@Component
public class PropertiesConfig {

    @Value("${campaign_api_address}")
    private String campaignAPIAddress;

    public String getCampaignAPIAddress() {
        return campaignAPIAddress;
    }

}
