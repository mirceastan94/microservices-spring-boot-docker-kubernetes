package com.pep.points.adapter;

import com.pep.points.dto.OrderDto;
import com.pep.points.dto.OrderItemDto;
import com.pep.points.dto.PointsTransactionDto;
import com.pep.points.dto.ProductDto;
import com.pep.points.entity.Order;
import com.pep.points.entity.OrderItem;
import com.pep.points.entity.PointsTransaction;
import com.pep.points.entity.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the adapter class utilized for points transaction entity/Dto types conversions
 */
public class PointsTransactionAdapter {

    /**
     * This method maps the input points transaction to a new Dto object
     *
     * @param pointsTransaction
     * @return
     */
    public static PointsTransactionDto toPointsTransactionDto(PointsTransaction pointsTransaction) {
        final Order orderEntity = pointsTransaction.getOrder();
        final List<OrderItem> itemsEntity = orderEntity.getItems();

        final List<OrderItemDto> orderItems = new ArrayList<>();

        itemsEntity.forEach(itemEntity -> {
            final Product product = itemEntity.getProduct();

            final ProductDto productDto = new ProductDto();
            productDto.setId(product.getId());
            productDto.setName(product.getName());
            productDto.setPrice(product.getPrice());

            final OrderItemDto orderItemDto = new OrderItemDto();
            orderItemDto.setId(itemEntity.getId());
            orderItemDto.setQuantity(itemEntity.getQuantity());
            orderItemDto.setProduct(productDto);

            orderItems.add(orderItemDto);
        });

        final OrderDto order = new OrderDto();
        order.setId(orderEntity.getId());
        order.setPoints(orderEntity.getPoints());
        order.setCreatedAt(orderEntity.getCreatedAt());
        order.setCustomerEmail(orderEntity.getCustomerEmail());
        order.setItems(orderItems);

        final PointsTransactionDto pointsTransactionDto = new PointsTransactionDto();
        pointsTransactionDto.setOrder(order);

        return pointsTransactionDto;
    }

}
