package com.pep.points.service.impl;

import com.pep.points.config.FeignClientConfig;
import com.pep.points.config.PropertiesConfig;
import com.pep.points.dto.CampaignDto;
import com.pep.points.dto.PointsTransactionDto;
import com.pep.points.dto.ProductInfoDto;
import com.pep.points.entity.*;
import com.pep.points.exception.BadRequestException;
import com.pep.points.exception.ResourceAlreadyExistsException;
import com.pep.points.exception.ResourceNotFoundException;
import com.pep.points.repository.*;
import com.pep.points.service.PointsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static com.pep.points.adapter.PointsTransactionAdapter.toPointsTransactionDto;
import static com.pep.points.utils.Constants.CAMPAIGN_ACTIVE_ON_DATE_URI;
import static com.pep.points.utils.Constants.DATE;

/**
 * This is the points service implementation
 */
@Service
@Transactional
public class PointsServiceImpl implements PointsService {

    @Autowired
    private PointsTransactionRepository pointsTransactionRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private PointsBalanceRepository pointsBalanceRepository;

    @Autowired(required = false)
    private RestTemplate restTemplate;

    @Autowired(required = false)
    private FeignClientConfig feignClient;

    @Autowired
    private PropertiesConfig propertiesConfig;

    @Override
    public PointsTransactionDto findPointsTransaction(UUID id) {
        PointsTransaction pointsTransactionEntity = pointsTransactionRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("PointsTransaction", "ID", id));

        // a custom DTO has been created for the points transaction response to avoid JSON marshalling limitations using Jackson
        return toPointsTransactionDto(pointsTransactionEntity);
    }

    @Override
    public void addPointsTransaction(PointsTransaction pointsTransaction) {
        // not enough fields to properly determine if a "PointsTransaction already exists", assuming the ID is not sent in the
        // POST request -> customerEmail + createdAt were used as a "placeholder", but if two transactions would be added in the
        // same day for the same customer email, one could get rejected, which is obviously not a good idea from a business perspective
        if (orderRepository.existsByCustomerEmailAndCreatedAt(pointsTransaction.getOrder().getCustomerEmail(),
                pointsTransaction.getOrder().getCreatedAt())) {
            throw new ResourceAlreadyExistsException("PointsTransaction", "customer email and creation time",
                    pointsTransaction.getOrder().getCustomerEmail() + " and " + pointsTransaction.getOrder().
                            getCreatedAt());
        }

        // calculates balance points for current order
        final Order orderDto = pointsTransaction.getOrder();
        final Integer orderPoints = calculateBalancePointsForProducts(orderDto);
        orderDto.setPoints(orderPoints);

        // if the same product (ID) has already been inserted for another points transaction, the name and price will be updated
        // the best implementation would be a call towards the campaignapi microservice, to the "real" products table;
        // however, that would modify the model proposed for this task by a bigger margin, as the pointsapi product table
        // would go away, and since the product is tied to a campaign in the other API, one would also need to store the
        // campaign for each product ordered (as it was actually done with the points number to avoid any calculus when
        // removing an order from balance), to know at which price to get it with
        updateProducts(orderDto);

        // inserts the points transaction

        final PointsTransaction newPointsTransaction = new PointsTransaction(orderDto);
        pointsTransactionRepository.save(newPointsTransaction);

        // checks whether the customer email has already been added, to either update the balance or create a new one
        assignBalancePoints(orderDto);

    }

    @Override
    public PointsTransaction editPointsTransaction(UUID id, PointsTransaction updatedPointsTransaction) {
        // verifies whether a points transaction already exists, as otherwise the update/edit operation would make no sense
        if (!pointsTransactionRepository.existsById(id)) {
            throw new ResourceNotFoundException("PointsTransaction", "ID", id);
        }

        // retrieves the existing points transaction
        final PointsTransaction currentPointsTransaction = pointsTransactionRepository.getOne(id);

        if (updatedPointsTransaction.getOrder() != null && updatedPointsTransaction.getOrder().getId() != null &&
                !(updatedPointsTransaction.getOrder().getId().equals(currentPointsTransaction.getOrder().getId()))) {
            throw new BadRequestException("Order ID cannot be altered!");
        }

        // gets the current and updated order objects
        final Order currentOrder = currentPointsTransaction.getOrder();
        final Order updatedOrder = updatedPointsTransaction.getOrder();

        // maps the newer information to the older entities
        currentOrder.setCreatedAt(updatedOrder.getCreatedAt());
        currentOrder.setCustomerEmail(updatedOrder.getCustomerEmail());

        // searches for the existing products to update the price for this campaign
        updateItemInfo(currentOrder, updatedOrder);

        // updates the order points based on the current campaigns and chosen products
        final Integer updatedOrderPoints = calculateBalancePointsForProducts(updatedOrder);

        // recalculates the points balance with the updated value
        final Optional<PointsBalance> optCustomerPointsBalance = pointsBalanceRepository.findByCustomerEmail(updatedPointsTransaction.getOrder().getCustomerEmail());
        if (!optCustomerPointsBalance.isPresent()) {
            throw new ResourceNotFoundException("Points balance", "customer email", updatedPointsTransaction.getOrder().getCustomerEmail());
        } else {
            final PointsBalance currentPointsBalance = optCustomerPointsBalance.get();
            currentPointsBalance.setPoints(currentPointsBalance.getPoints() - currentPointsTransaction.getOrder().getPoints() +
                    updatedOrderPoints);

            pointsBalanceRepository.save(currentPointsBalance);
        }

        // updates the order points for further possible updates in the future
        // by maintaining this column, one can immediately know what the older order points count was,
        // without recalculating everything in terms of original prices/points/quantity
        currentOrder.setPoints(updatedOrderPoints);

        return pointsTransactionRepository.save(currentPointsTransaction);
    }

    @Override
    public void removePointsTransactionById(UUID id) {
        final PointsTransaction searchedTransaction = pointsTransactionRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Transaction", "ID", id));
        final Order searchedOrder = searchedTransaction.getOrder();
        deductPointsFromCustomerBalance(searchedOrder.getCustomerEmail(), searchedOrder.getPoints());

        pointsTransactionRepository.delete(searchedTransaction);
    }

    @Override
    public void removePointsTransactionByOrderId(UUID id) {
        final Order searchedOrder = orderRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("Transaction", "ID", id));

        deductPointsFromCustomerBalance(searchedOrder.getCustomerEmail(), searchedOrder.getPoints());

        pointsTransactionRepository.delete (searchedOrder.getPointsTransaction());
    }

    @Override
    public PointsBalance findPointsBalanceByCustomerEmail(String customerEmail) {
        return pointsBalanceRepository.findByCustomerEmail(customerEmail).orElseThrow(() ->
                new ResourceNotFoundException("Points balance", "customer email", customerEmail));
    }

    private void deductPointsFromCustomerBalance(String customerEmail, Integer points) {
        final Optional<PointsBalance> optCustomerPointsBalance = pointsBalanceRepository.findByCustomerEmail(customerEmail);
        if (optCustomerPointsBalance.isPresent()) {
            final PointsBalance pointsBalance = optCustomerPointsBalance.get();
            pointsBalance.setPoints(pointsBalance.getPoints() - points);
            pointsBalanceRepository.save(pointsBalance);
        }
    }

    private Integer calculateBalancePointsForProducts(Order orderDto) {
        final Map<String, Object> parametersMap = new HashMap<>();
        parametersMap.put(DATE, orderDto.getCreatedAt());

        // gets the active campaigns from the campaign API
        final ResponseEntity<CampaignDto[]> response;
        if (feignClient != null) {
            response = getExchangeWithFeign(feignClient, orderDto.getCreatedAt());
        } else {
            response = getExchangeWithTemplate(restTemplate, parametersMap);
        }

        final CampaignDto[] campaigns = response.getBody();
        final List<CampaignDto> campaignDtos = Arrays.asList(campaigns);

        // calculates the pricing information map for each product of each campaign
        final Map<UUID, ProductInfoDto> campaignProductsMap = new HashMap<>();
        campaignDtos.forEach(campaignDto ->
                campaignDto.getProducts().forEach(productDto ->
                        campaignProductsMap.putIfAbsent(productDto.getId(), new ProductInfoDto(productDto.getPrice(), campaignDto.getPoints()))
                ));

        // computes the balance points based on the generated products information
        // each product found in the order which is also part of an active campaign will be calculated accordingly,
        // with the campaign price and points multiplier
        final AtomicReference<Integer> pointsCalculated = new AtomicReference<>(0);
        orderDto.getItems().forEach(orderItem -> {
            if (campaignProductsMap.containsKey(orderItem.getProduct().getId())) {
                final ProductInfoDto productInfoDto = campaignProductsMap.get(orderItem.getProduct().getId());
                pointsCalculated.updateAndGet(v -> v + productInfoDto.getPrice().intValueExact() * productInfoDto.getPoints() * orderItem.getQuantity());
            } else {
                pointsCalculated.updateAndGet(v -> v + orderItem.getProduct().getPrice().intValueExact() * orderItem.getQuantity());
            }
        });

        return pointsCalculated.get();
    }

    @Profile("dev")
    private ResponseEntity<CampaignDto[]> getExchangeWithTemplate(RestTemplate restTemplate, Map<String, Object> parametersMap) {
        return restTemplate.exchange(propertiesConfig.getCampaignAPIAddress().concat(CAMPAIGN_ACTIVE_ON_DATE_URI),
                HttpMethod.GET, null, CampaignDto[].class, parametersMap);
    }

    @Profile("prod")
    private ResponseEntity<CampaignDto[]> getExchangeWithFeign(FeignClientConfig feignClient, LocalDate createdAtDate) {
        return feignClient.getActiveCampaignsByDate(createdAtDate);
    }

    private void updateProducts(Order orderDto) {
        orderDto.getItems().forEach(orderItem -> {
            final Product currentProduct = orderItem.getProduct();
            if (productRepository.existsById(currentProduct.getId())) {
                productRepository.save(currentProduct);
            }
        });
    }

    private void updateItemInfo(Order currentOrder, Order updatedOrder) {
        updatedOrder.getItems().forEach(orderItem -> {
            if (orderItemRepository.existsById(orderItem.getId())) {
                final OrderItem persistedItem = orderItemRepository.getOne(orderItem.getId());
                persistedItem.setQuantity(orderItem.getQuantity());
                if (orderItem.getProduct() != null) {
                    final Product updatedProduct = orderItem.getProduct();
                    final Product persistedProduct = productRepository.getOne(orderItem.getProduct().getId());
                    persistedProduct.setName(updatedProduct.getName());
                    persistedProduct.setPrice(updatedProduct.getPrice());
                    persistedProduct.getItems().add(persistedItem);
                    persistedItem.setProduct(persistedProduct);
                    currentOrder.getItems().remove(orderItem);
                    currentOrder.getItems().add(persistedItem);
                }
            }
        });
    }

    private void assignBalancePoints(Order orderDto) {
        final Optional<PointsBalance> optCustomerPointsBalance = pointsBalanceRepository.findByCustomerEmail(orderDto.getCustomerEmail());
        if (optCustomerPointsBalance.isPresent()) {
            final PointsBalance currentPointsBalance = optCustomerPointsBalance.get();
            currentPointsBalance.setPoints(currentPointsBalance.getPoints() + orderDto.getPoints());
            pointsBalanceRepository.save(currentPointsBalance);
        } else {
            final PointsBalance newPointsBalance = new PointsBalance();
            newPointsBalance.setCustomerEmail(orderDto.getCustomerEmail());
            newPointsBalance.setPoints(orderDto.getPoints());
            pointsBalanceRepository.save(newPointsBalance);
        }
    }

}
