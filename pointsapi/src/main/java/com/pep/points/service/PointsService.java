package com.pep.points.service;

import com.pep.points.dto.PointsTransactionDto;
import com.pep.points.entity.PointsBalance;
import com.pep.points.entity.PointsTransaction;

import java.util.UUID;

/**
 * This is the points service interface
 */
public interface PointsService {

    PointsTransactionDto findPointsTransaction(UUID id);

    void addPointsTransaction(PointsTransaction pointsTransaction);

    PointsTransaction editPointsTransaction(UUID id, PointsTransaction pointsTransaction);

    void removePointsTransactionById(UUID id);

    void removePointsTransactionByOrderId(UUID id);

    PointsBalance findPointsBalanceByCustomerEmail(String customerEmail);

}
