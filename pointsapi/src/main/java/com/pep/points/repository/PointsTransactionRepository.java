package com.pep.points.repository;

import com.pep.points.entity.PointsTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * This is the points transaction entity JpaRepository interface
 */
public interface PointsTransactionRepository extends JpaRepository<PointsTransaction, UUID> {}
