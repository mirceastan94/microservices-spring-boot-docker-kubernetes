package com.pep.points.repository;

import com.pep.points.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.UUID;

/**
 * This is the order entity JpaRepository interface
 */
public interface OrderRepository extends JpaRepository<Order, UUID> {

    boolean existsByCustomerEmailAndCreatedAt(String customerEmail, LocalDate createdAt);

}
