package com.pep.points.repository;

import com.pep.points.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * This is the product entity JpaRepository interface
 */
public interface ProductRepository extends JpaRepository<Product, UUID> {
}
