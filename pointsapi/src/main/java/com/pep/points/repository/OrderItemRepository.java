package com.pep.points.repository;

import com.pep.points.entity.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * This is the order item entity JpaRepository interface
 */
public interface OrderItemRepository extends JpaRepository<OrderItem, UUID> {

}
