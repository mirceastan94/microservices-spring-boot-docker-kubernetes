package com.pep.points.repository;

import com.pep.points.entity.PointsBalance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

/**
 * This is the points balance entity JpaRepository interface
 */
public interface PointsBalanceRepository extends JpaRepository<PointsBalance, UUID> {

    Optional<PointsBalance> findByCustomerEmail(String customerEmail);

}
