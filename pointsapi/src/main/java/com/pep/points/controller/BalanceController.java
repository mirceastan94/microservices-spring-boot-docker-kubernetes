package com.pep.points.controller;

import com.pep.points.service.PointsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;

/**
 * This class is the controller for points balance related operations
 */
@RestController
@RequestMapping("/balance")
public class BalanceController {

    @Autowired
    private PointsService pointsService;

    @GetMapping(value = "/customer/{customeremail}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPointsBalanceByCustomerEmail(@NotBlank @PathVariable(value = "customeremail") String customerEmail) {
        return ResponseEntity.ok(pointsService.findPointsBalanceByCustomerEmail(customerEmail));
    }

}
