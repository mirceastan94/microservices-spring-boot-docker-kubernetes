package com.pep.points.controller;

import com.pep.points.entity.PointsTransaction;
import com.pep.points.service.PointsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

/**
 * This class is the controller for points transactions related operations
 */
@RestController
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private PointsService pointsService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPointsTransactionById(@NotBlank @PathVariable(value = "id") UUID id) {
        return ResponseEntity.ok(pointsService.findPointsTransaction(id));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createPointsTransaction(@Valid @RequestBody PointsTransaction newPointsTransactionDto) {
        pointsService.addPointsTransaction(newPointsTransactionDto);

        return new ResponseEntity<>("PointsTransaction created", HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updatePointsTransaction(@NotBlank @PathVariable(value = "id") UUID id,
                                                     @Valid @RequestBody PointsTransaction updatedPointsTransaction) {
        pointsService.editPointsTransaction(id, updatedPointsTransaction);

        return new ResponseEntity<>("PointsTransaction updated", HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deletePointsTransactionById(@NotBlank @PathVariable(value = "id") UUID id) {
        pointsService.removePointsTransactionById(id);

        return new ResponseEntity<>("Transaction deleted", HttpStatus.OK);
    }

    @DeleteMapping(value = "/order/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deletePointsTransactionByOrderId(@NotBlank @PathVariable(value = "id") UUID id) {
        pointsService.removePointsTransactionByOrderId(id);

        return new ResponseEntity<>("Transaction deleted", HttpStatus.OK);
    }

}
