package com.pep.points.utils;

/**
 * This is the constants variables class
 */
public class Constants {

    public static final String CAMPAIGN_ACTIVE_ON_DATE_URI = "/active/{date}";
    public static final String DATE = "date";
}
