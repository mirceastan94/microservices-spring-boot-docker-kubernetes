package com.pep.campaign.controller;

import com.pep.campaign.dto.CampaignDto;
import com.pep.campaign.entity.Campaign;
import com.pep.campaign.service.CampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.UUID;

/**
 * This class is the controller for campaign related operations
 */
@RestController
@RequestMapping("/")
public class CampaignController {

    @Autowired
    private CampaignService campaignService;

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllCampaigns() {
        return ResponseEntity.ok(campaignService.findAllCampaigns());
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCampaignById(@NotBlank @PathVariable(value = "id") UUID id) {
        return ResponseEntity.ok(campaignService.findCampaignById(id));
    }

    @GetMapping(value = "/active/{date}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getActiveCampaignsByDate(@NotBlank @PathVariable(value = "date")
                                                      @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        return ResponseEntity.ok(campaignService.findCampaignsByDate(date));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createCampaign(@Valid @RequestBody CampaignDto newCampaignDto) {
        campaignService.addCampaignWithProducts(newCampaignDto);

        return new ResponseEntity<>("Campaign created", HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateCampaign(@NotBlank @PathVariable(value = "id") UUID id,
                                            @Valid @RequestBody CampaignDto updatedCampaignDto) {
        Campaign updatedCampaign = campaignService.editCampaign(id, updatedCampaignDto);

        return new ResponseEntity<>(updatedCampaign, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteCampaignById(@NotBlank @PathVariable(value = "id") UUID id) {
        campaignService.removeCampaign(id);

        return new ResponseEntity<>("Campaign deleted", HttpStatus.OK);
    }

}
