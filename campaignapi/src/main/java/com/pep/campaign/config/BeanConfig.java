package com.pep.campaign.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * This class is a test that consists of setting a specific bean for multiple environments
 */
@Configuration
public class BeanConfig {

    private static final Log logger = LogFactory.getLog(BeanConfig.class);


    @Profile("dev")
    @Bean
    public void devBean() {
        logger.info("Writing from DEV");
    }

    @Profile("prod")
    @Bean
    public void prodBean() {
        logger.info("Writing from PROD");
    }

}
