package com.pep.campaign.service;

import com.pep.campaign.dto.CampaignDto;
import com.pep.campaign.entity.Campaign;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

/**
 * This is the campaign service interface
 */
public interface CampaignService {

    List<Campaign> findAllCampaigns();

    List<CampaignDto> findCampaignsByDate(LocalDate date);

    Campaign findCampaignById(UUID id);

    void addCampaignWithProducts(CampaignDto newCampaignDto);

    Campaign editCampaign(UUID id, CampaignDto updatedCampaignDto);

    void removeCampaign(UUID id);

}
