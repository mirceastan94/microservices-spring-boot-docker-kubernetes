package com.pep.campaign.service.impl;

import com.pep.campaign.dto.CampaignDto;
import com.pep.campaign.entity.Campaign;
import com.pep.campaign.entity.Product;
import com.pep.campaign.exception.ResourceNotFoundException;
import com.pep.campaign.repository.CampaignRepository;
import com.pep.campaign.repository.ProductRepository;
import com.pep.campaign.service.CampaignService;
import com.pep.campaign.validation.CampaignValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static com.pep.campaign.adapter.CampaignAdapter.*;

/**
 * This is the campaign service implementation
 */
@Service
@Transactional
public class CampaignServiceImpl implements CampaignService {

    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CampaignValidator campaignValidator;

    @Override
    public List<Campaign> findAllCampaigns() {
        return campaignRepository.findAll();
    }

    @Override
    public List<CampaignDto> findCampaignsByDate(LocalDate date) {
        List<Campaign> campaigns = campaignRepository.findAllByStartDateBeforeAndEndDateAfter(date, date);

        return toNewCampaignDtos(campaigns);
    }

    @Override
    public Campaign findCampaignById(UUID id) {
        return campaignRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Campaign", "ID", id));
    }

    @Override
    public void addCampaignWithProducts(CampaignDto newCampaignDto) {
        // validates the user input
        campaignValidator.validateCampaign(newCampaignDto, campaignRepository);

        // instantiates a new Campaign object
        Campaign newCampaign = toNewCampaign(newCampaignDto);

        // persists the newly generated instance in the database
        campaignRepository.save(newCampaign);
    }

    @Override
    public Campaign editCampaign(UUID id, CampaignDto updatedCampaignDto) {
        // validates the user input
        campaignValidator.validateCampaign(updatedCampaignDto, campaignRepository);

        // looks the existing campaign up and updates its properties
        Campaign currentCampaign = campaignRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Campaign", "ID", id));
        toUpdatedCampaign(currentCampaign, updatedCampaignDto);

        // searches for the existing products to update the price for this campaign
        updatedCampaignDto.getProducts().forEach(dtoProduct -> {
            if (productRepository.existsByNameAndCampaigns(dtoProduct.getName(), currentCampaign)) {
                Product persistedProduct = productRepository.findByNameAndCampaignsEquals(dtoProduct.getName(), currentCampaign);
                persistedProduct.setPrice(dtoProduct.getPrice());
                currentCampaign.getProducts().remove(dtoProduct);
                currentCampaign.getProducts().add(persistedProduct);
            }
        });

        // persists the updated campaign information
        return campaignRepository.save(currentCampaign);
    }

    @Override
    public void removeCampaign(UUID id) {
        Campaign searchedCampaign = campaignRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Campaign", "ID", id));

        campaignRepository.delete(searchedCampaign);
    }

}
