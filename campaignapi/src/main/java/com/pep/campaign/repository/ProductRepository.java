package com.pep.campaign.repository;

import com.pep.campaign.entity.Campaign;
import com.pep.campaign.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * This is the campaign entity JpaRepository interface
 */
public interface ProductRepository extends JpaRepository<Product, UUID> {

    boolean existsByNameAndCampaigns(String name, Campaign campaign);

    Product findByNameAndCampaignsEquals(String name, Campaign campaign);
}
