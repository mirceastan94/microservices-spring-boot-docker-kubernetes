package com.pep.campaign.repository;

import com.pep.campaign.entity.Campaign;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

/**
 * This is the campaign entity JpaRepository interface
 */
public interface CampaignRepository extends JpaRepository<Campaign, UUID> {

    List<Campaign> findAllByStartDateBeforeAndEndDateAfter(LocalDate startDate, LocalDate endDate);

    boolean existsByNameAndStartDateEqualsAndEndDateEquals(String name, LocalDate startDate, LocalDate endDate);
}
