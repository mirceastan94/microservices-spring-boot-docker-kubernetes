package com.pep.campaign.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
public class ProductDto {

    @NotNull
    private UUID id;

    @NotBlank
    private String name;

    @NotNull
    @Positive
    private BigDecimal price;
}
