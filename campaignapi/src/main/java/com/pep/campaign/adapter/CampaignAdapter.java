package com.pep.campaign.adapter;

import com.pep.campaign.dto.ProductDto;
import com.pep.campaign.entity.Campaign;
import com.pep.campaign.dto.CampaignDto;
import com.pep.campaign.entity.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the adapter class utilized for campaign entity/Dto types conversions
 */
public class CampaignAdapter {

    /**
     * This method maps the input campaign Dto to a new entity object
     *
     * @param campaignDto
     * @return
     */
    public static Campaign toNewCampaign(CampaignDto campaignDto) {
        Campaign newCampaign = new Campaign();
        newCampaign.setName(campaignDto.getName());
        newCampaign.setStartDate(campaignDto.getStartDate());
        newCampaign.setEndDate(campaignDto.getEndDate());
        newCampaign.setPoints(campaignDto.getPoints());
        campaignDto.getProducts().forEach(productDto -> {
            Product newProduct = new Product();
            newProduct.setName(productDto.getName());
            newProduct.setPrice(productDto.getPrice());
            newCampaign.getProducts().add(newProduct);
        });

        return newCampaign;
    }

    /**
     * This method maps the input campaign to a dto object
     *
     * @param campaigns
     * @return
     */
    public static List<CampaignDto> toNewCampaignDtos(List<Campaign> campaigns) {
        final List<CampaignDto> campaignDtos = new ArrayList<>();
        campaigns.forEach(campaign -> {
            CampaignDto newCampaignDto = new CampaignDto();
            newCampaignDto.setId(campaign.getId());
            newCampaignDto.setName(campaign.getName());
            newCampaignDto.setStartDate(campaign.getStartDate());
            newCampaignDto.setEndDate(campaign.getEndDate());
            newCampaignDto.setPoints(campaign.getPoints());
            campaign.getProducts().forEach(product -> {
                ProductDto newProductDto = new ProductDto();
                newProductDto.setId(product.getId());
                newProductDto.setName(product.getName());
                newProductDto.setPrice(product.getPrice());
                newCampaignDto.getProducts().add(newProductDto);
            });

            campaignDtos.add(newCampaignDto);
        });

        return campaignDtos;
    }

    /**
     * This method updates an existing campaign entity entry with newer details
     *
     * @param currentCampaign
     * @param updatedCampaignDto
     * @return
     */
    public static Campaign toUpdatedCampaign(Campaign currentCampaign, CampaignDto updatedCampaignDto) {
        currentCampaign.setName(updatedCampaignDto.getName());
        currentCampaign.setStartDate(updatedCampaignDto.getStartDate());
        currentCampaign.setEndDate(updatedCampaignDto.getEndDate());
        currentCampaign.setPoints(updatedCampaignDto.getPoints());

        return currentCampaign;
    }

}
