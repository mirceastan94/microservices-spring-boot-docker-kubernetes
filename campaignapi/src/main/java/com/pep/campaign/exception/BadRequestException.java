package com.pep.campaign.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This class defines the exception processing for a bad request sent to the service
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

    public BadRequestException(String requestError) {
        super(String.format("Bad request: " + requestError));
    }
}
