package com.pep.campaign.validation;

import com.pep.campaign.dto.CampaignDto;
import com.pep.campaign.exception.BadRequestException;
import com.pep.campaign.exception.ResourceAlreadyExistsException;
import com.pep.campaign.repository.CampaignRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * This is the component class for additional campaign insert/update validations
 */
@Component
public class CampaignValidator {

    public void validateCampaign(CampaignDto newCampaignDto, CampaignRepository campaignRepository) {
        if (campaignRepository.existsByNameAndStartDateEqualsAndEndDateEquals(newCampaignDto.getName(), newCampaignDto.getStartDate(), newCampaignDto.getEndDate())) {
            throw new ResourceAlreadyExistsException("Campaign", "name and dates period", newCampaignDto.getName());
        }
        if (newCampaignDto.getStartDate().isAfter(newCampaignDto.getEndDate())) {
            throw new BadRequestException("Campaign end date must be greater than or equal to start date!");
        }
        if (newCampaignDto.getStartDate().isBefore(LocalDate.now().plusDays(1))) {
            throw new BadRequestException("Campaign start date must be greater than or equal to tomorrow!");
        }
    }
}
