package com.pep.campaign;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This class starts the campaign application
 *
 * @author Mircea Stan
 */
@SpringBootApplication
public class CampaignApplication implements CommandLineRunner {

    private static final Log logger = LogFactory.getLog(CampaignApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(CampaignApplication.class, args);
    }

    public void run(String... args) {
        logger.info("Campaign service has started!");
    }

}
