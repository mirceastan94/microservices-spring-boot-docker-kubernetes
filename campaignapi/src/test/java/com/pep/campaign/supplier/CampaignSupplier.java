package com.pep.campaign.supplier;

import com.pep.campaign.dto.CampaignDto;
import com.pep.campaign.dto.ProductDto;
import com.pep.campaign.entity.Campaign;
import com.pep.campaign.entity.Product;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public class CampaignSupplier {

    public static Optional<Campaign> supplyCampaignForGet() {
        final Campaign campaign = new Campaign();
        campaign.setId(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c391"));
        campaign.setName("TestCampaign");
        campaign.setPoints(5);
        campaign.setStartDate(LocalDate.of(2020, 10, 20));
        campaign.setEndDate(LocalDate.of(2021, 12, 30));

        final Product product = new Product();
        product.setId(UUID.randomUUID());
        product.setName("TestProduct");
        product.setPrice(BigDecimal.TEN);

        final Set<Product> productSet = new HashSet<>();
        productSet.add(product);
        campaign.setProducts(productSet);

        return Optional.of(campaign);
    }

    public static CampaignDto supplyCampaignDtoForInsert() {
        final CampaignDto campaignDto = new CampaignDto();
        campaignDto.setName("TestCampaign");
        campaignDto.setPoints(5);
        campaignDto.setStartDate(LocalDate.of(2020, 06, 14));
        campaignDto.setEndDate(LocalDate.of(2021, 12, 30));

        final ProductDto product = new ProductDto();
        product.setId(UUID.randomUUID());
        product.setName("TestProduct");
        product.setPrice(BigDecimal.TEN);

        final Set<ProductDto> productSet = new HashSet<>();
        productSet.add(product);
        campaignDto.setProducts(productSet);

        return campaignDto;
    }

    public static CampaignDto supplyCampaignDtoForUpdate() {
        final CampaignDto campaignDto = new CampaignDto();
        campaignDto.setName("UpdatedCampaign");
        campaignDto.setPoints(7);
        campaignDto.setStartDate(LocalDate.of(2020, 10, 20));
        campaignDto.setEndDate(LocalDate.of(2021, 12, 30));

        final ProductDto product = new ProductDto();
        product.setId(UUID.randomUUID());
        product.setName("TestProduct");
        product.setPrice(BigDecimal.ONE);

        final Set<ProductDto> productSet = new HashSet<>();
        productSet.add(product);
        campaignDto.setProducts(productSet);

        return campaignDto;
    }

    public static Product supplyProductForUpdate() {
        final Product product = new Product();
        product.setId(UUID.randomUUID());
        product.setName("TestProduct");
        product.setPrice(BigDecimal.TEN);
        return product;
    }
}
