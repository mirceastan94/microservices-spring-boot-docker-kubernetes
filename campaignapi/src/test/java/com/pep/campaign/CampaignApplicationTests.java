package com.pep.campaign;

import com.pep.campaign.dto.CampaignDto;
import com.pep.campaign.entity.Campaign;
import com.pep.campaign.entity.Product;
import com.pep.campaign.exception.BadRequestException;
import com.pep.campaign.repository.CampaignRepository;
import com.pep.campaign.repository.ProductRepository;
import com.pep.campaign.service.CampaignService;
import com.pep.campaign.service.impl.CampaignServiceImpl;
import com.pep.campaign.supplier.CampaignSupplier;
import com.pep.campaign.validation.CampaignValidator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static com.pep.campaign.adapter.CampaignAdapter.toNewCampaign;
import static com.pep.campaign.adapter.CampaignAdapter.toUpdatedCampaign;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.AssertionErrors.assertNotNull;

/**
 * This class defines the unit tests for CampaignAPI
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class CampaignApplicationTests {

    public CampaignApplicationTests() {
    }

    @Mock
    private CampaignRepository campaignRepository = mock(CampaignRepository.class);

    @Mock
    private ProductRepository productRepository = mock(ProductRepository.class);

    @Mock
    private CampaignValidator campaignValidator = mock(CampaignValidator.class);

    @InjectMocks
    private CampaignService campaignService = new CampaignServiceImpl();

    @Test
    public void getCampaignById() {
        final Optional<Campaign> suppliedCampaign = CampaignSupplier.supplyCampaignForGet();

        doReturn(suppliedCampaign).when(campaignRepository).findById(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c391"));

        Campaign searchedCampaign = campaignService.findCampaignById(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c391"));
        assertNotNull("Searched campaign is null!", searchedCampaign);
        assertEquals(searchedCampaign, suppliedCampaign.get());
    }

    @Test(expected = BadRequestException.class)
    public void addCampaignInvalidStartDate() {
        final CampaignDto suppliedCampaign = CampaignSupplier.supplyCampaignDtoForInsert();
        doThrow(BadRequestException.class).when(campaignValidator).validateCampaign(suppliedCampaign, campaignRepository);
        campaignService.addCampaignWithProducts(suppliedCampaign);
    }

    @Test(expected = BadRequestException.class)
    public void addCampaignInvalidStartDateAfterEndDate() {
        final CampaignDto suppliedCampaign = CampaignSupplier.supplyCampaignDtoForInsert();
        suppliedCampaign.setStartDate(LocalDate.of(2023, 1, 3));
        doThrow(BadRequestException.class).when(campaignValidator).validateCampaign(suppliedCampaign, campaignRepository);
        campaignService.addCampaignWithProducts(suppliedCampaign);
    }

    @Test
    public void addCampaignWithProducts() {
        final CampaignDto suppliedCampaign = CampaignSupplier.supplyCampaignDtoForInsert();
        doReturn(toNewCampaign(suppliedCampaign)).when(campaignRepository).save(toNewCampaign(suppliedCampaign));
        campaignService.addCampaignWithProducts(suppliedCampaign);
    }

    @Test
    public void editCampaignWithProducts() {
        final Campaign originalCampaign = toNewCampaign(CampaignSupplier.supplyCampaignDtoForInsert());
        final CampaignDto suppliedCampaignDto = CampaignSupplier.supplyCampaignDtoForUpdate();
        final Campaign suppliedUpdatedCampaign = toNewCampaign(suppliedCampaignDto);
        final Product suppliedProduct = CampaignSupplier.supplyProductForUpdate();

        doReturn(Optional.of(originalCampaign)).when(campaignRepository).findById(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c391"));
        doReturn(true).when(productRepository).existsByNameAndCampaigns("TestProduct", originalCampaign);
        doReturn(suppliedProduct).when(productRepository).findByNameAndCampaignsEquals("TestProduct", originalCampaign);
        doReturn(toNewCampaign(suppliedCampaignDto)).when(campaignRepository).save(toUpdatedCampaign(originalCampaign, suppliedCampaignDto));

        Campaign updatedCampaign = campaignService.editCampaign(UUID.fromString("e7ca7ef1-e4c7-46ac-9a72-a31023c6c391"), suppliedCampaignDto);

        assertNotNull("Updated campaign is null!", updatedCampaign);
        final Product updatedProduct = updatedCampaign.getProducts().stream().findFirst().get();
        assertEquals(updatedProduct.getName(), suppliedProduct.getName());
        assertEquals(updatedProduct.getPrice(), suppliedProduct.getPrice());
        assertEquals(updatedCampaign.getStartDate(), suppliedUpdatedCampaign.getStartDate());
        assertEquals(updatedCampaign.getEndDate(), suppliedUpdatedCampaign.getEndDate());
        assertEquals(updatedCampaign.getName(), suppliedUpdatedCampaign.getName());
        assertEquals(updatedCampaign.getPoints(), suppliedUpdatedCampaign.getPoints());
    }

}
